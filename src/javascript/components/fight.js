import {keyDownHandler, keyUpHandler} from './eventHandlers';

export const state = {
    firstFighter: {},
    secondFighter: {},
    firstFighterCurrentHealth: null,
    secondFighterCurrentHealth: null,
    initialWidthHealthIndicator: null,
    isFirstFighterBlockActivated: false,
    isSecondFighterBlockActivated: false,
    timeFirstFighterCriticalHit: null,
    timeSecondFighterCriticalHit: null,
    firstFighterKeysArray: [],
    secondFighterKeysArray: []
}


export async function fight(firstFighter, secondFighter) {

    console.log(firstFighter.defense)

    setInit(firstFighter, secondFighter);

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
      document.addEventListener('keydown', keyDownHandler.bind(null, firstFighter, secondFighter, resolve));
      document.addEventListener('keyup', keyUpHandler);
  });
}

export function getDamage(attacker, defender) {
    const hitPower = getHitPower(attacker);
    const blockPower = getBlockPower(defender);
    const damage = hitPower - blockPower;
    return (damage < 0) ? 0 : damage;
}

export function getHitPower(fighter) {
    if (!fighter) return 0;
  const Min = 1;
  const Max = 2;

  const criticalHitChance = Math.random() * (Max - Min) + Min;
  const power = +fighter.attack * criticalHitChance;

  return power;
}

export function getBlockPower(fighter) {
    if (!fighter) return 0;
  const Min = 1;
  const Max = 2;

  const dodgeChance = Math.random() * (Max - Min) + Min;
  const power = +fighter.defense * dodgeChance;

  return power;
}

export function getCriticalHitPower(fighter) {
    return fighter.attack * 2;
}

export function decrHealth(isFirstFightAtta, damage){
    const currHealth = isFirstFightAtta ? 'secondFighterCurrentHealth' : 'firstFighterCurrentHealth'
    state[currHealth] -= damage;
}

export function decreasHealIndic(isFirstFightAtta) {
    const defen = isFirstFightAtta ? state.secondFighter : state.firstFighter;
    const currHealth = isFirstFightAtta ? state.secondFighterCurrentHealth : state.firstFighterCurrentHealth;
    const pos = isFirstFightAtta ? 'right' : 'left';

    const healthIndc = document.getElementById(`${pos}-fighter-indicator`);

    const newWidth = state.initialWidthHealthIndicator * currHealth / defen.health;
    healthIndc.style.width = newWidth > 0 ? newWidth + 'px' : 0;
}

function setInit(firstFighter, secondFighter) {
    state.firstFighter = firstFighter;
    state.secondFighter = secondFighter;
    state.firstFighterCurrentHealth = firstFighter.health;
    state.secondFighterCurrentHealth = secondFighter.health;

    const healthIndc = document.getElementById(`left-fighter-indicator`);
    state.initialWidthHealthIndicator = healthIndc.clientWidth;
}