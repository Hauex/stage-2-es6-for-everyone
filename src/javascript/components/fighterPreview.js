import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });


  if (fighter) {
    const fighterImage = createFighterImage(fighter);
    const detailsContainer = createElement({tagName: 'div', className: 'fighter-preview___details'});
    detailsContainer.insertAdjacentHTML('beforeend', createFighterInfo(fighter));

    fighterElement.append(fighterImage, detailsContainer);
  }


  return fighterElement;
}

export function createFighterInfo(fighter) {
    return `<h1>${fighter.name}</h1>
      <b><span>${fighter.health}</span></b>
      <b><span>${fighter.attack}</span></b>
      <b><span>${fighter.defense}</span></b>`;

}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
